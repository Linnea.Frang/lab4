package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
	
	int cols;
	int rows;
	CellState[][] CellGridMatrix;

    public CellGrid(int rows, int columns, CellState initialState) {
    	this.rows = rows;
    	this.cols = columns;
    	this.CellGridMatrix = new CellState[rows][columns];
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
    	if(row < 0 || row > numRows() || column < 0 || column > numColumns())
    		throw new IndexOutOfBoundsException("Utenfor grid");
    	CellGridMatrix[row][column] = element;   
    }

    @Override
    public CellState get(int row, int column) {
    	if(row < 0 || row > numRows() || column < 0 || column > numColumns())
    		throw new IndexOutOfBoundsException("Utenfor grid");
        return CellGridMatrix[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid CelleGrid = new CellGrid(this.rows, this.cols, CellState.DEAD);
        for(int row = 0; row<this.rows; row++) {
        	for(int col = 0; col<this.cols; col++) {
        		CelleGrid.set(row, col, get(row, col));
        	}
        }
        return CelleGrid;
    }
    
}
